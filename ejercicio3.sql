﻿USE ciclistas;

-- 1 edades de todos los ciclistas
        SELECT c.edad FROM ciclista c;
        
-- 2 edades de ciclistas de banesto o navigare
        SELECT c.edad, c.nomequipo FROM ciclista c WHERE c.nomequipo= 'Banesto' OR c.nomequipo= 'Navigare';
        
-- 3 listar el dorsal de los ciclistas que son del banesto y cuya edad está entre 25 y 32
        SELECT c.dorsal, c.edad FROM ciclista c  WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32; 
        
-- 4 listar la inicial del equipo cuyos nombres de los ciclistas al que pertenecen empiecen por r
      SELECT DISTINCT LEFT( c.nomequipo,1), c.nombre FROM ciclista c WHERE c.nombre LIKE 'r%' ;
      
-- 5 listar el codigo de las etapas que su salida y llegada sea en la misma poblacion
        SELECT e.numetapa, e.salida, e.llegada FROM etapa e WHERE e.salida= e.llegada;
        
-- 6 listar el codigo de las etapas que su salida y llegada no sean en la misma poblacion y 
         -- que conozcamos el dorsal del ciclista que ha ganado la etapa 
      SELECT e.numetapa, e.dorsal FROM etapa e WHERE e.salida <> e.llegada AND e.dorsal IS NOT NULL; 
      
-- 7 listar el nombre de los puertos cuya altura este entre 1000 y 2000 o que la altura sea mayor que 2400
         SELECT p.nompuerto, p.altura FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura >2400;
        
-- 8 listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura este entre 1000 y 2000
      -- o que la altura sea mayor que 2400
      SELECT p.dorsal, p.nompuerto, p.altura FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura >2400; 
      
-- 9 listar el numero de ciclistas que hayan ganado alguna etapa
      SELECT COUNT( DISTINCT e.dorsal) ciclistasGanadores FROM etapa e;
      -- con subconsulta 
      SELECT DISTINCT e.dorsal FROM etapa e ;                           
         SELECT  COUNT(*) ciclistasGanadores FROM (SELECT DISTINCT e.dorsal FROM etapa e) AS c1; 

-- 10 listar el numero de etapas que tenga el puerto
      SELECT DISTINCT p.numetapa FROM puerto p; 

-- 11 listar el numero de ciclistas que hayan ganado algun puerto
    -- con subconsulta
       SELECT DISTINCT p.dorsal FROM puerto p; 
      SELECT COUNT(*) FROM (SELECT DISTINCT p.dorsal FROM puerto p) c1;

-- 12 listar codigo de la etapa con el numero de puertos que tiene
      SELECT p.numetapa,COUNT(*) puerto FROM  puerto p GROUP BY p.numetapa;
    
-- 13 indicar la altura media de los puertos
       SELECT AVG( p.altura) FROM puerto p; 
    
-- 14 indicar el codigo de etapa cuya altura media de sus puertos esta por encima de 1500
       SELECT  p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG( p.altura) >1500 ;
    
-- 15 indicar el numero de etapas que cumplen la condicion anterior
          SELECT  p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG( p.altura) >1500 ;
          SELECT COUNT(*) FROM ( SELECT  p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG( p.altura) >1500) c1;

-- 16 listar el dorsal del ciclista con el numero de veces que ha llevado algun millot
         SELECT l.dorsal, COUNT(*) numeroDeVeces FROM lleva l GROUP BY l.dorsal, l.código; 

-- 17 listar el dorsal, el numero de etapa, el ciclista y el numero de maillots que ese ciclista ha llevado en cada etapa
        SELECT COUNT(*) maillotEtapa, l.dorsal, l.numetapa FROM lleva l; 
