﻿/* creacion de la base de datos */
DROP DATABASE IF EXISTS ciclistas;
CREATE DATABASE IF NOT EXISTS ciclistas 
  CHARACTER SET utf8 
  COLLATE utf8_spanish_ci;

/* codificacion en transmision castellano */
SET NAMES 'utf8';

/* SELECCIONAMOS LA BASE DE DATOS */
USE ciclistas;



/* CREAR TABLA EQUIPO */
CREATE TABLE IF NOT EXISTS equipo (
  -- CREAMOS LOS CAMPOS
  nomequipo varchar(25) NOT NULL,
  director varchar(30) DEFAULT NULL,
  -- COLOCAMOS RESTRICCIONES, CLAVES Y CHECKS
  PRIMARY KEY (nomequipo)
)
  -- OPCIONES DE LA TABLA
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci; 

/* crear tabla maillot */
CREATE TABLE IF NOT EXISTS maillot (
-- CREAR CAMPOS
  código varchar(3) NOT NULL,
  tipo varchar(30) NOT NULL,
  color varchar(20) NOT NULL,
  premio int(10) NOT NULL,
  -- RESTRICCIONES
  PRIMARY KEY (código)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

/* CREAR TABLA CICLISTA */

CREATE TABLE IF NOT EXISTS ciclista (
  -- CREAMOS LOS CAMPOS
  dorsal smallint(5) NOT NULL,
  nombre varchar(30) NOT NULL,
  edad smallint(5) DEFAULT NULL,
  nomequipo varchar(25) NOT NULL,
  -- RESTRICCIONES
  PRIMARY KEY (dorsal), -- CLAVE PRINCIPAL
  INDEX ciclistanomequipo (nomequipo), -- INDEXADO CON DUPLICADOS
  CONSTRAINT fk_ciclista_equipo 
      FOREIGN KEY (nomequipo) REFERENCES equipo(nomequipo) 
      ON DELETE CASCADE ON UPDATE CASCADE -- CLAVE AJENA
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci; 

/* creamos la tabla etapa */
CREATE TABLE IF NOT EXISTS etapa (
  -- campos
  numetapa smallint(5) NOT NULL,
  kms smallint(5) NOT NULL,
  salida varchar(35) NOT NULL,
  llegada varchar(35) NOT NULL,
  dorsal smallint(5) DEFAULT NULL,
  -- restricciones
  PRIMARY KEY (numetapa),
  CONSTRAINT fk_etapa_ciclista FOREIGN KEY (dorsal) REFERENCES 
    ciclista(dorsal) ON DELETE CASCADE ON UPDATE CASCADE   
)
  -- opciones de tabla
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

/* crear tabla lleva */
CREATE TABLE IF NOT EXISTS lleva (
  -- campos
  dorsal smallint(5) NOT NULL,
  numetapa smallint(5) NOT NULL,
  código varchar(3) NOT NULL,
  -- restricciones
  PRIMARY KEY (numetapa, código),
  INDEX FK_lleva_etapa_numetapa (numetapa),
  CONSTRAINT FK_lleva_etapa_numetapa FOREIGN KEY (numetapa)
    REFERENCES ciclistas.etapa (numetapa) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_lleva_maillot_código FOREIGN KEY (código)
    REFERENCES ciclistas.maillot (código) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_lleva_ciclista FOREIGN KEY (dorsal)
    REFERENCES ciclista (dorsal) ON DELETE CASCADE ON UPDATE CASCADE
)
  -- opciones
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;



/* CRER TABLA PUERTO */
CREATE TABLE IF NOT EXISTS puerto (
  nompuerto varchar(35) NOT NULL,
  altura smallint(5) NOT NULL,
  categoria varchar(1) NOT NULL,
  pendiente double(15, 5) DEFAULT NULL,
  numetapa smallint(5) NOT NULL,
  dorsal smallint(5) DEFAULT NULL,
  PRIMARY KEY (nompuerto),
  CONSTRAINT fk_puerto_etapa FOREIGN KEY (numetapa) 
    REFERENCES etapa(numetapa) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_puerto_ciclista FOREIGN KEY (dorsal) 
    REFERENCES ciclista (dorsal) ON DELETE CASCADE ON UPDATE CASCADE     
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

